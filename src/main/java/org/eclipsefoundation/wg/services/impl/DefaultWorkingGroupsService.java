/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		  Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.services.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.wg.models.WorkingGroupMap;
import org.eclipsefoundation.wg.models.WorkingGroupMap.WorkingGroup;
import org.eclipsefoundation.wg.models.WorkingGroupMap.WorkingGroupParticipationAgreement;
import org.eclipsefoundation.wg.services.WorkingGroupsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.runtime.Startup;

/**
 * Builds a list of working group definitions from an embedded list of working group definitions. This is an interim
 * solution to accelerate this project and should be replaced with a call to the foundation API to retrieve this data.
 * 
 * @author Martin Lowe, Zachary Sabourin
 */
@Startup
@ApplicationScoped
public class DefaultWorkingGroupsService implements WorkingGroupsService {
    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultWorkingGroupsService.class);

    @ConfigProperty(name = "eclipse.working-groups.filepath")
    String filepath;

    @Inject
    ObjectMapper json;

    private Map<String, WorkingGroup> workingGroups;

    /**
     * At startup, will load the working groups and store them locally to be used throughout the servers life time. As this
     * resource is embedded within the Jar, we do not need to look for changes to the resource, as that would not happen
     * with a production server.
     * 
     * @throws IOException if there is an issue loading the working groups resource from within the Jar resources.
     */
    @PostConstruct
    void init() throws IOException {
        LOGGER.info("Starting init of working group levels static members");

        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(filepath)) {
            WorkingGroupMap map = json.readValue(is, WorkingGroupMap.class);
            this.workingGroups = new HashMap<>(map.getWorkingGroups());
            LOGGER.info("Initialized {} working group", workingGroups.size());
        }
    }

    @Override
    public Set<WorkingGroup> get(List<String> projectStatuses) {
        return new HashSet<>(workingGroups
                .entrySet()
                .stream()
                .filter(e -> filterByProjectStatuses(projectStatuses, e.getValue()))
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue))
                .values());
    }

    @Override
    public WorkingGroup getByAlias(String alias) {
        return workingGroups.get(alias);
    }

    @Override
    public Map<String, List<String>> getWGPADocumentIDs() {
        Map<String, List<String>> wgToDocument = new HashMap<>();
        workingGroups.values().stream().forEach(wg -> wgToDocument.put(wg.getAlias(), extractWGPADocumentIDs(wg)));
        return wgToDocument;
    }

    private boolean filterByProjectStatuses(List<String> statuses, WorkingGroup wg) {
        if (statuses == null || statuses.isEmpty()) {
            return true;
        }
        return statuses.contains(wg.getStatus());
    }

    private List<String> extractWGPADocumentIDs(WorkingGroup wg) {
        List<String> ids = new ArrayList<>();
        WorkingGroupParticipationAgreement iwgpa = wg.getResources().getParticipationAgreements().getIndividual();
        if (iwgpa != null) {
            ids.add(iwgpa.getDocumentId());
        }

        WorkingGroupParticipationAgreement wgpa = wg.getResources().getParticipationAgreements().getOrganization();
        if (wgpa != null) {
            ids.add(wgpa.getDocumentId());
        }

        return ids;
    }
}
