/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		  Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.models;

import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * JSON object for importing Working group JSON assets.
 * 
 * @author Martin Lowe, Zachary Sabourin
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_WorkingGroupMap.Builder.class)
public abstract class WorkingGroupMap {
    public abstract Map<String, WorkingGroup> getWorkingGroups();

    public static Builder builder() {
        return new AutoValue_WorkingGroupMap.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setWorkingGroups(Map<String, WorkingGroup> workingGroups);

        public abstract WorkingGroupMap build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_WorkingGroupMap_WorkingGroup.Builder.class)
    public abstract static class WorkingGroup {
        public abstract String getAlias();

        public abstract String getTitle();

        public abstract String getStatus();

        public abstract String getLogo();

        public abstract String getDescription();

        public abstract String getParentOrganization();

        public abstract WorkingGroupResources getResources();

        public abstract List<WorkingGroupParticipationLevel> getLevels();

        public static Builder builder() {
            return new AutoValue_WorkingGroupMap_WorkingGroup.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setAlias(String alias);

            public abstract Builder setTitle(String title);

            public abstract Builder setStatus(String status);

            public abstract Builder setLogo(String logo);

            public abstract Builder setDescription(String description);

            public abstract Builder setParentOrganization(String parentOrganization);

            public abstract Builder setResources(WorkingGroupResources resources);

            public abstract Builder setLevels(List<WorkingGroupParticipationLevel> levels);

            public abstract WorkingGroup build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_WorkingGroupMap_WorkingGroupResources.Builder.class)
    public abstract static class WorkingGroupResources {
        public abstract String getCharter();

        @JsonProperty("participation_agreements")
        public abstract WorkingGroupParticipationAgreements getParticipationAgreements();

        public abstract String getWebsite();

        public abstract String getMembers();

        public abstract String getSponsorship();

        public abstract String getContactForm();

        public static Builder builder() {
            return new AutoValue_WorkingGroupMap_WorkingGroupResources.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setCharter(String charter);

            public abstract Builder setParticipationAgreements(
                    WorkingGroupParticipationAgreements participationAgreements);

            public abstract Builder setWebsite(String website);

            public abstract Builder setMembers(String members);

            public abstract Builder setSponsorship(String sponsorship);

            public abstract Builder setContactForm(String contactForm);

            public abstract WorkingGroupResources build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_WorkingGroupMap_WorkingGroupParticipationLevel.Builder.class)
    public abstract static class WorkingGroupParticipationLevel {
        public abstract String getRelation();

        public abstract String getDescription();

        public static Builder builder() {
            return new AutoValue_WorkingGroupMap_WorkingGroupParticipationLevel.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setRelation(String relation);

            public abstract Builder setDescription(String description);

            public abstract WorkingGroupParticipationLevel build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_WorkingGroupMap_WorkingGroupParticipationAgreement.Builder.class)
    public abstract static class WorkingGroupParticipationAgreement {
        @JsonProperty("document_id")
        public abstract String getDocumentId();

        public abstract String getPdf();

        public static Builder builder() {
            return new AutoValue_WorkingGroupMap_WorkingGroupParticipationAgreement.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setDocumentId(String documentId);

            public abstract Builder setPdf(String pdf);

            public abstract WorkingGroupParticipationAgreement build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_WorkingGroupMap_WorkingGroupParticipationAgreements.Builder.class)
    public abstract static class WorkingGroupParticipationAgreements {
        @Nullable
        public abstract WorkingGroupParticipationAgreement getIndividual();

        @Nullable
        public abstract WorkingGroupParticipationAgreement getOrganization();

        public static Builder builder() {
            return new AutoValue_WorkingGroupMap_WorkingGroupParticipationAgreements.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setIndividual(@Nullable WorkingGroupParticipationAgreement individual);

            public abstract Builder setOrganization(@Nullable WorkingGroupParticipationAgreement organization);

            public abstract WorkingGroupParticipationAgreements build();
        }
    }
}
