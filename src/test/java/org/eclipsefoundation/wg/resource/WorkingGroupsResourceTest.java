/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.resource;

import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.templates.RestAssuredTemplates;
import org.eclipsefoundation.testing.templates.RestAssuredTemplates.EndpointTestCase;
import org.eclipsefoundation.wg.test.helpers.SchemaNamespaceHelper;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

@QuarkusTest
class WorkingGroupsResourceTest {

    public final static String WGS_BASE_URL = "/working-groups";

    public final static String WG_STATUS_URL = WGS_BASE_URL + "?status={param}";
    public final static String WG_STATUSES_URL = WGS_BASE_URL + "?status={param1}&status={param2}";

    public final static String WG_BASE_URL = WGS_BASE_URL + "/{alias}";

    public final static String WG_RESOURCES_URL = WG_BASE_URL + "/resources";
    public final static String WG_LEVELS_URL = WG_BASE_URL + "/levels";
    public final static String WG_AGREEMENTS_URL = WG_BASE_URL + "/agreements";

    /*
     * GET_ALL
     */
    public final static EndpointTestCase GET_ALL_SUCCESS = TestCaseHelper.buildSuccessCase(WGS_BASE_URL,
            new String[] {}, SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH);

    public final static EndpointTestCase GET_ALL_INVALID_FORMAT = TestCaseHelper.buildInvalidFormatCase(WGS_BASE_URL,
            new String[] {}, ContentType.TEXT);

    public final static EndpointTestCase GET_ALL_SINGLE_STATUS_SUCCESS = TestCaseHelper.buildSuccessCase(WG_STATUS_URL,
            new String[] { "active" }, SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH);

    public final static EndpointTestCase GET_ALL_MULTI_STATUS_SUCCESS = TestCaseHelper.buildSuccessCase(WG_STATUSES_URL,
            new String[] { "active", "archived" }, SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH);

    /*
     * GET_BY_ALIAS
     */
    public final static EndpointTestCase GET_BY_ALIAS_SUCCESS = TestCaseHelper.buildSuccessCase(WG_BASE_URL,
            new String[] { "awful-group" }, SchemaNamespaceHelper.WORKING_GROUP_SCHEMA_PATH);

    public final static EndpointTestCase GET_BY_ALIAS_INVALID_ALIAS = TestCaseHelper.buildNotFoundCase(WG_BASE_URL,
            new String[] { "invalid-Group" });

    public final static EndpointTestCase GET_BY_ALIAS_INVALID_FORMAT = TestCaseHelper.buildInvalidFormatCase(
            WG_BASE_URL, new String[] { "important-group" }, ContentType.TEXT);

    /*
     * GET_RESOURCES
     */
    public final static EndpointTestCase GET_RESOURCES_SUCCESS = TestCaseHelper.buildSuccessCase(WG_RESOURCES_URL,
            new String[] { "important-group" }, SchemaNamespaceHelper.WORKING_GROUP_RESOURCES_SCHEMA_PATH);

    public final static EndpointTestCase GET_RESOURCES_INVALID_ALIAS = TestCaseHelper.buildNotFoundCase(
            WG_RESOURCES_URL, new String[] { "invalid-Group" });

    public final static EndpointTestCase GET_RESOURCES_INVALID_FORMAT = TestCaseHelper.buildInvalidFormatCase(
            WG_RESOURCES_URL, new String[] { "mediocre-group " }, ContentType.TEXT);

    /*
     * GET_LEVELS
     */
    public final static EndpointTestCase GET_LEVELS_SUCCESS = TestCaseHelper.buildSuccessCase(WG_LEVELS_URL,
            new String[] { "mediocre-group" }, SchemaNamespaceHelper.WORKING_GROUP_LEVELS_SCHEMA_PATH);

    public final static EndpointTestCase GET_LEVELS_INVALID_ALIAS = TestCaseHelper.buildNotFoundCase(WG_LEVELS_URL,
            new String[] { "invalid-Group" });

    public final static EndpointTestCase GET_LEVELS_INVALID_FORMAT = TestCaseHelper.buildInvalidFormatCase(
            WG_LEVELS_URL, new String[] { "mediocre-group " }, ContentType.TEXT);

    /*
     * GET_AGREEMENTS
     */
    public final static EndpointTestCase GET_AGREEMENTS_SUCCESS = TestCaseHelper.buildSuccessCase(WG_AGREEMENTS_URL,
            new String[] { "important-group" }, SchemaNamespaceHelper.WORKING_GROUP_AGREEMENT_SCHEMA_PATH);

    public final static EndpointTestCase GET_AGREEMENTS_INVALID_ALIAS = TestCaseHelper.buildNotFoundCase(
            WG_AGREEMENTS_URL, new String[] { "invalid-Group" });

    public final static EndpointTestCase GET_AGREEMENTS_INVALID_FORMAT = TestCaseHelper.buildInvalidFormatCase(
            WG_AGREEMENTS_URL, new String[] { "important-group" }, ContentType.TEXT);

    @Test
    void getAll_success() {
        RestAssuredTemplates.testGet(GET_ALL_SUCCESS);
    }

    @Test
    void getAll_success_validResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_ALL_SUCCESS);
    }

    @Test
    void getAll_success_matchingSpec() {
        RestAssuredTemplates.testGet_validateSchema(GET_ALL_SUCCESS);
    }

    @Test
    void getAll_failure_invalidFormat() {
        RestAssuredTemplates.testGet(GET_ALL_INVALID_FORMAT);
    }

    @Test
    void getAllBySingleStatus_success() {
        RestAssuredTemplates.testGet(GET_ALL_SINGLE_STATUS_SUCCESS);
    }

    @Test
    void getAllBySingleStatus_success_validResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_ALL_SINGLE_STATUS_SUCCESS);
    }

    @Test
    void getAllBySingleStatus_success_matchingSpec() {
        RestAssuredTemplates.testGet_validateSchema(GET_ALL_SINGLE_STATUS_SUCCESS);
    }

    @Test
    void getAllByMultiStatus_success() {
        RestAssuredTemplates.testGet(GET_ALL_MULTI_STATUS_SUCCESS);
    }

    @Test
    void getAllByMultiStatus_success_validResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_ALL_MULTI_STATUS_SUCCESS);
    }

    @Test
    void getAllByMultiStatus_success_matchingSpec() {
        RestAssuredTemplates.testGet_validateSchema(GET_ALL_MULTI_STATUS_SUCCESS);
    }

    @Test
    void getByAlias_success() {
        RestAssuredTemplates.testGet(GET_BY_ALIAS_SUCCESS);
    }

    @Test
    void getByAlias_success_validResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_BY_ALIAS_SUCCESS);
    }

    @Test
    void getByAlias_success_matchingSpec() {
        RestAssuredTemplates.testGet_validateSchema(GET_BY_ALIAS_SUCCESS);
    }

    @Test
    void getByAlias_failure_invalidName() {
        RestAssuredTemplates.testGet(GET_BY_ALIAS_INVALID_ALIAS);
    }

    @Test
    void getByAlias_failure_invalidFormat() {
        RestAssuredTemplates.testGet(GET_BY_ALIAS_INVALID_FORMAT);
    }

    @Test
    void getResources_success() {
        RestAssuredTemplates.testGet(GET_RESOURCES_SUCCESS);
    }

    @Test
    void getResources_success_validResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_RESOURCES_SUCCESS);
    }

    @Test
    void getResources_success_matchingSpec() {
        RestAssuredTemplates.testGet_validateSchema(GET_RESOURCES_SUCCESS);
    }

    @Test
    void getResources_failure_invalidName() {
        RestAssuredTemplates.testGet(GET_RESOURCES_INVALID_ALIAS);
    }

    @Test
    void getResources_failure_invalidFormat() {
        RestAssuredTemplates.testGet(GET_RESOURCES_INVALID_FORMAT);
    }

    @Test
    void getLevels_success() {
        RestAssuredTemplates.testGet(GET_LEVELS_SUCCESS);
    }

    @Test
    void getLevels_success_validResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_LEVELS_SUCCESS);
    }

    @Test
    void getLevels_success_matchingSpec() {
        RestAssuredTemplates.testGet_validateSchema(GET_LEVELS_SUCCESS);
    }

    @Test
    void getLevels_failure_invalidName() {
        RestAssuredTemplates.testGet(GET_LEVELS_INVALID_ALIAS);
    }

    @Test
    void getLevels_failure_invalidFormat() {
        RestAssuredTemplates.testGet(GET_LEVELS_INVALID_FORMAT);
    }

    @Test
    void getAgreements_success() {
        RestAssuredTemplates.testGet(GET_AGREEMENTS_SUCCESS);
    }

    @Test
    void getAgreements_success_validResponseFormat() {
        RestAssuredTemplates.testGet_validateResponseFormat(GET_AGREEMENTS_SUCCESS);
    }

    @Test
    void getAgreements_success_matchingSpec() {
        RestAssuredTemplates.testGet_validateSchema(GET_AGREEMENTS_SUCCESS);
    }

    @Test
    void getAgreements_failure_invalidName() {
        RestAssuredTemplates.testGet(GET_AGREEMENTS_INVALID_ALIAS);
    }

    @Test
    void getAgreements_failure_invalidFormat() {
        RestAssuredTemplates.testGet(GET_AGREEMENTS_INVALID_FORMAT);
    }
}
